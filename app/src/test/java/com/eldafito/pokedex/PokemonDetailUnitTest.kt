package com.eldafito.pokedex

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.eldafito.pokedex.model.PokemonDetail
import com.eldafito.pokedex.repository.PokemonRepository
import com.eldafito.pokedex.viewmodel.PokemonDetailViewModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class PokemonDetailUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: PokemonDetailViewModel

    private val pokemon = PokemonDetail(1, "poke test", listOf(), listOf(), listOf())
    private val initialState = PokemonDetailViewModel.State(isIdle = true)
    private val pokemonRepository = mock<PokemonRepository> {
        onBlocking { fetchPokemonDetail(1) } doReturn pokemon
    }

    private val observer = mock<Observer<PokemonDetailViewModel.State>>()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = PokemonDetailViewModel(initialState, pokemon.id, pokemonRepository)
        viewModel.observableState.observeForever(observer)
    }

    @Test
    fun loadDetail() {
        val activeState = initialState.copy(pokemon = pokemon, isIdle = false)
        verify(observer).onChanged(activeState)
        verifyNoMoreInteractions(observer)
    }

}