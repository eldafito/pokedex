package com.eldafito.pokedex

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.paging.PagingData
import com.eldafito.pokedex.model.Pokemon
import com.eldafito.pokedex.repository.PokemonRepository
import com.eldafito.pokedex.viewmodel.PokemonListViewModel
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.flow.flow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class PokemonListUnitTest {

    companion object {
        private const val PAGE_SIZE = 20
    }

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: PokemonListViewModel

    private val initialState = PokemonListViewModel.State(isIdle = true)

    private val fakePokemonList = listOf(Pokemon(1, "test"))
    private val fakePagingData = PagingData.from(fakePokemonList)
    private val pokemonRepository = mock<PokemonRepository> {
        onBlocking { pokemon(PAGE_SIZE) } doReturn flow {
            emit(fakePagingData)
        }
    }

    private val observer = mock<Observer<PokemonListViewModel.State>>()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = PokemonListViewModel(
            initialState = initialState,
            pagingSize = PAGE_SIZE,
            pokemonRepository = pokemonRepository
        )
        viewModel.observableState.observeForever(observer)
    }

    @Test
    fun loadPaging() {
        val activeState = initialState.copy(pokemonPagingData = fakePagingData, isIdle = false)
        verify(observer).onChanged(activeState)
        verifyNoMoreInteractions(observer)
    }

}