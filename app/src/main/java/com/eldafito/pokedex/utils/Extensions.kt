package com.eldafito.pokedex.utils

import android.content.Intent

private const val EXTRA_POKEMON_ID = "extra_pokemon_id"

var Intent.pokemonId: Int?
    get() = getIntExtra(EXTRA_POKEMON_ID, -1)
    set(value) {
        value?.let { putExtra(EXTRA_POKEMON_ID, it) }
    }

