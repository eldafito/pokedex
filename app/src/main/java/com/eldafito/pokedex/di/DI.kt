package com.eldafito.pokedex.di

import com.eldafito.pokedex.remote.PokeApi
import com.eldafito.pokedex.repository.PokemonRepository
import com.eldafito.pokedex.repository.PokemonRepositoryDefault
import com.eldafito.pokedex.viewmodel.PokemonDetailViewModel
import com.eldafito.pokedex.viewmodel.PokemonListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private const val PAGING_SIZE = 50

val appModule = module {
    single { PokeApi.create() }
    single<PokemonRepository> { PokemonRepositoryDefault(pokeApi = get()) }
    viewModel {
        PokemonListViewModel(
            initialState = null,
            pagingSize = PAGING_SIZE,
            pokemonRepository = get()
        )
    }
    viewModel { (pokemonId: Int) ->
        PokemonDetailViewModel(
            initialState = null,
            pokemonId = pokemonId,
            pokemonRepository = get()
        )
    }
}