package com.eldafito.pokedex.viewmodel

import com.eldafito.pokedex.model.PokemonDetail
import com.eldafito.pokedex.repository.PokemonRepository
import com.eldafito.pokedex.viewmodel.PokemonDetailViewModel.*
import cz.levinzonr.roxie.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow

class PokemonDetailViewModel(
    initialState: State? = null,
    private val pokemonId: Int,
    private val pokemonRepository: PokemonRepository
) :
    RoxieViewModel<Action, State, Change>() {

    override val initialState = initialState ?: State(isIdle = true)

    override val reducer: Reducer<State, Change> = { state, change ->
        when (change) {
            Change.Loading -> state.copy(
                isIdle = false,
                isLoading = true,
                error = null,
                pokemon = null
            )
            is Change.Detail -> state.copy(
                isIdle = false,
                isLoading = false,
                error = null,
                pokemon = change.pokemon
            )
            is Change.Error -> state.copy(
                isIdle = false,
                isLoading = false,
                error = change.throwable,
                pokemon = null
            )
        }
    }

    init {
        startActionsObserver()
        dispatch(Action.Load)
    }

    override fun emitAction(action: Action): Flow<Change> =
        when (action) {
            Action.Load -> channelFlow {
                offer(Change.Loading)
                try {
                    offer(Change.Detail(pokemonRepository.fetchPokemonDetail(pokemonId)))
                } catch (e: Exception) {
                    offer(Change.Error(e))
                }
            }
        }

    sealed class Action : BaseAction {
        object Load : Action()
    }

    sealed class Change : BaseChange {
        object Loading : Change()
        data class Detail(val pokemon: PokemonDetail) : Change()
        data class Error(val throwable: Throwable?) : Change()
    }

    data class State(
        val pokemon: PokemonDetail? = null,
        val isIdle: Boolean = false,
        val isLoading: Boolean = false,
        val error: Throwable? = null
    ) : BaseState

}
