package com.eldafito.pokedex.viewmodel

import androidx.paging.PagingData
import com.eldafito.pokedex.model.Pokemon
import com.eldafito.pokedex.repository.PokemonRepository
import com.eldafito.pokedex.viewmodel.PokemonListViewModel.*
import cz.levinzonr.roxie.*
import kotlinx.coroutines.flow.*

class PokemonListViewModel(
    initialState: State?,
    private val pagingSize: Int,
    private val pokemonRepository: PokemonRepository
) :
    RoxieViewModel<Action, State, Change>() {

    override val initialState = initialState ?: State(isIdle = true)

    override val reducer: Reducer<State, Change> = { state, change ->
        when (change) {
            Change.Loading -> state.copy(
                pokemonPagingData = null,
                isIdle = false,
                isLoading = true,
                error = null
            )
            is Change.PokemonList -> state.copy(
                pokemonPagingData = change.pagingData,
                isIdle = false,
                isLoading = false,
                error = null
            )
            is Change.Error -> state.copy(
                pokemonPagingData = null,
                isIdle = false,
                isLoading = false,
                error = change.error
            )
        }
    }

    init {
        startActionsObserver()
        dispatch(Action.Load)
    }

    override fun emitAction(action: Action): Flow<Change> =
        when (action) {
            Action.Load -> channelFlow {
                pokemonRepository.pokemon(pagingSize)
                    .onStart { offer(Change.Loading) }
                    .catch { offer(Change.Error(it)) }
                    .collectLatest {
                        offer(Change.PokemonList(it))
                    }
            }
        }

    sealed class Action : BaseAction {
        object Load : Action()
    }

    sealed class Change : BaseChange {
        object Loading : Change()
        data class PokemonList(val pagingData: PagingData<Pokemon>) : Change()
        data class Error(val error: Throwable?) : Change()
    }

    data class State(
        val pokemonPagingData: PagingData<Pokemon>? = null,
        val isIdle: Boolean = false,
        val isLoading: Boolean = false,
        val error: Throwable? = null
    ) : BaseState

}
