package com.eldafito.pokedex.ui

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import com.eldafito.pokedex.model.PokemonDetail
import com.eldafito.pokedex.model.PokemonDetail.Type
import com.eldafito.pokedex.utils.pokemonId
import com.eldafito.pokedex.viewmodel.PokemonDetailViewModel
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_pokemon_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class PokemonDetailActivity : AppCompatActivity() {

    private val imgs = mutableListOf<String>()
    private val galleryAdapter = PokemonGalleryAdapter(imgs)

    private val types = mutableListOf<Type>()
    private val typesAdapter = PokemonTypesAdapter(types)

    private val stats = mutableListOf<PokemonDetail.Stat>()
    private val statsAdapter = PokemonStatsAdapter(stats)

    private val viewModel by viewModel<PokemonDetailViewModel>(parameters = {
        parametersOf(
            intent.pokemonId
        )
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_detail)
        pokemon_images_viewpager.adapter = galleryAdapter

        pokemon_types_recycler.let { r ->
            r.adapter = typesAdapter
            r.setHasFixedSize(true)
            r.addItemDecoration(HorizontalItemDecorator(resources.getDimensionPixelSize(R.dimen.spacing_l)))
        }

        pokemon_stats_recycler.let { r ->
            r.adapter = statsAdapter
            r.setHasFixedSize(true)
        }

        TabLayoutMediator(pokemon_images_tablyt, pokemon_images_viewpager,
            TabLayoutMediator.TabConfigurationStrategy { _, _ ->
            }).attach()

        viewModel.observableState.observe(this, Observer { state ->
            imgs.clear()
            types.clear()
            stats.clear()
            imgs.addAll(state.pokemon?.images.orEmpty())
            types.addAll(state.pokemon?.types.orEmpty())
            stats.addAll(state.pokemon?.stats.orEmpty())
            galleryAdapter.notifyDataSetChanged()
            typesAdapter.notifyDataSetChanged()
            statsAdapter.notifyDataSetChanged()
            pokemon_images_viewpager.isVisible = imgs.isNotEmpty()
            pokemon_types_recycler.isVisible = types.isNotEmpty()
            pokemon_stats_recycler.isVisible = types.isNotEmpty()

            title = state.pokemon?.name
            pokemon_error_txt.text = state.error?.localizedMessage
            pokemon_error_txt.isVisible = state.error != null
            pokemon_progress.isVisible = state.isLoading
        })
    }

    inner class HorizontalItemDecorator(private val spacing: Int) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val count = parent.adapter?.itemCount ?: 0
            val pos = parent.getChildAdapterPosition(view)
            outRect.right = if (pos < count - 1) spacing else 0
        }

    }

}