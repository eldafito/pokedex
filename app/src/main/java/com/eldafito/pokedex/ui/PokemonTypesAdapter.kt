package com.eldafito.pokedex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import com.eldafito.pokedex.model.PokemonDetail.Type
import kotlinx.android.synthetic.main.item_pokemon_type.view.*

class PokemonTypesAdapter(private val types: List<Type>) :
    RecyclerView.Adapter<PokemonTypesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon_type, parent, false)
        )

    override fun getItemCount() = types.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(types[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val emojiTxtView = itemView.pokemon_type_emoji
        private val titleTxtView = itemView.pokemon_type_title

        fun bind(type: Type) {
            titleTxtView.text = type.name
            emojiTxtView.text = type.emoji
        }
    }

}