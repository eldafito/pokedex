package com.eldafito.pokedex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import com.squareup.picasso.Picasso

class PokemonGalleryAdapter(private val imgs: List<String>) :
    RecyclerView.Adapter<PokemonGalleryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon_img, parent, false)
        )

    override fun getItemCount() = imgs.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(imgs[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imgView = itemView as ImageView

        fun bind(img: String) {
            Picasso.get().load(img).into(imgView)
        }
    }

}