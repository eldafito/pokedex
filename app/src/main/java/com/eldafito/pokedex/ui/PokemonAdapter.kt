package com.eldafito.pokedex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import com.eldafito.pokedex.model.Pokemon
import kotlinx.android.synthetic.main.item_pokemon.view.*
import java.util.*

class PokemonAdapter(diffCallback: DiffUtil.ItemCallback<Pokemon>) :
    PagingDataAdapter<Pokemon, PokemonAdapter.ViewHolder>(diffCallback) {

    var pokemonSelectionListener: ((Pokemon) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val titeTxt = itemView.pokemon_title_txt

        fun bind(pokemon: Pokemon?) {
            titeTxt.text = pokemon?.name?.capitalize(Locale.getDefault())
            itemView.setOnClickListener {
                pokemon?.let { p -> pokemonSelectionListener?.invoke(p) }
            }
        }
    }
}