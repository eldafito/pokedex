package com.eldafito.pokedex.ui

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.eldafito.pokedex.R
import kotlinx.android.synthetic.main.widget_stat.view.*

class StatView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var title: String? = null
        set(value) {
            field = value
            post {
                stat_title_txt.text = value
            }
        }

    var progress = 0
        set(value) {
            field = value
            post {
                stat_progress.progress = value
                refreshMsg()
            }
        }

    var baseValue = 0
        set(value) {
            field = value
            post {
                stat_progress.max = value
                refreshMsg()
            }
        }

    init {
        inflate(context, R.layout.widget_stat, this)
    }

    private fun refreshMsg() {
        val msg = "$progress / $baseValue"
        stat_msg_txt.text = msg
    }

}