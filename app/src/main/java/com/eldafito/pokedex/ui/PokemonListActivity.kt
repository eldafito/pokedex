package com.eldafito.pokedex.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.eldafito.pokedex.R
import com.eldafito.pokedex.utils.pokemonId
import com.eldafito.pokedex.viewmodel.PokemonListViewModel
import kotlinx.android.synthetic.main.activity_pokemon_list.*
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class PokemonListActivity : AppCompatActivity() {

    private val viewModel: PokemonListViewModel by viewModel()

    private val adapter = PokemonAdapter(PokemonComparator)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_list)
        pokemon_list_recycler.also { r ->
            r.adapter = adapter.withLoadStateFooter(LoadingAdapter())
            r.setHasFixedSize(true)
        }

        adapter.pokemonSelectionListener = { p ->
            Intent(this, PokemonDetailActivity::class.java).also { i ->
                i.pokemonId = p.id
                startActivity(i)
            }
        }

        viewModel.observableState.observe(this, Observer { state ->
            state.pokemonPagingData?.let { pagingData ->
                lifecycleScope.launch {
                    adapter.submitData(pagingData)
                }
            }
            pokemon_list_recycler.isVisible = state.pokemonPagingData != null
            pokemon_list_progress.isVisible = state.isLoading
            pokemon_list_error_txt.text = state.error?.localizedMessage
            pokemon_list_error_txt.isVisible = state.error != null
        })
    }
}