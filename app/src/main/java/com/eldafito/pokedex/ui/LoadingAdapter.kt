package com.eldafito.pokedex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import kotlinx.android.synthetic.main.item_header_footer.view.*

class LoadingAdapter : LoadStateAdapter<LoadingAdapter.LoadStateViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_header_footer, parent, false)
        return LoadStateViewHolder(view)
    }

    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) =
        holder.bind(loadState)

    class LoadStateViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val progressBar: ProgressBar = itemView.header_footer_progress
        private val errorMsg: TextView = itemView.header_footer_msg_txt

        fun bind(loadState: LoadState) {
            errorMsg.text = (loadState as? LoadState.Error)?.error?.localizedMessage
            progressBar.isVisible = loadState is LoadState.Loading
            errorMsg.isVisible = loadState is LoadState.Error
        }
    }

}

