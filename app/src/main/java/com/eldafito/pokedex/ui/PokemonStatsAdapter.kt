package com.eldafito.pokedex.ui

import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.eldafito.pokedex.R
import com.eldafito.pokedex.model.PokemonDetail.Stat
import java.util.*

class PokemonStatsAdapter(private val stats: List<Stat>) :
    RecyclerView.Adapter<PokemonStatsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(StatView(parent.context).apply {
            layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT,
                resources.getDimensionPixelSize(R.dimen.stat_item_height)
            )
        })

    override fun getItemCount() = stats.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(stats[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val statView = itemView as StatView

        fun bind(stat: Stat) {
            statView.apply {
                title = stat.name.capitalize(Locale.getDefault())
                progress = stat.value
                baseValue = stat.baseValue
            }
        }
    }

}