package com.eldafito.pokedex.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.eldafito.pokedex.model.Pokemon
import com.eldafito.pokedex.model.PokemonDetail
import com.eldafito.pokedex.remote.PokeApi
import com.eldafito.pokedex.remote.response.PokemonRemote
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

interface PokemonRepository {

    fun pokemon(pageSize: Int): Flow<PagingData<Pokemon>>

    suspend fun fetchPokemonDetail(pokemonId: Int): PokemonDetail

}

class PokemonRepositoryDefault(private val pokeApi: PokeApi) : PokemonRepository {

    override fun pokemon(pageSize: Int): Flow<PagingData<Pokemon>> =
        Pager(config = PagingConfig(pageSize = pageSize)) { NetworkPagingSource(pokeApi) }.flow

    override suspend fun fetchPokemonDetail(pokemonId: Int): PokemonDetail =
        suspendCoroutine { cont ->
            pokeApi.getPokemon(pokemonId)
                .enqueue(object : Callback<PokemonRemote> {

                    override fun onFailure(call: Call<PokemonRemote>, t: Throwable) {
                        cont.resumeWithException(t)
                    }

                    override fun onResponse(
                        call: Call<PokemonRemote>,
                        response: Response<PokemonRemote>
                ) {
                    response.takeIf { it.isSuccessful }?.body()?.let { body ->
                        Timber.d("Fetched pokemon detail -> $body")
                        val pokemon = PokemonDetail.fromRemote(body)
                        cont.resume(pokemon)
                    } ?: cont.resumeWithException(Exception("No pokemon"))
                }

            })
    }

}