package com.eldafito.pokedex.repository

import android.net.Uri
import androidx.paging.PagingSource
import com.eldafito.pokedex.model.Pokemon
import com.eldafito.pokedex.remote.PokeApi

class NetworkPagingSource(private val pokeApi: PokeApi) : PagingSource<String, Pokemon>() {

    override suspend fun load(params: LoadParams<String>): LoadResult<String, Pokemon> {
        return try {
            val uri = params.key?.let { Uri.parse(it) }
            val offset = uri?.getQueryParameter("offset")?.toInt() ?: 0
            val response = pokeApi.getPokemonList(limit = params.loadSize, offset = offset)
            LoadResult.Page(
                data = response.results.mapNotNull { Pokemon.fromRemote(it) },
                prevKey = response.previous,
                nextKey = response.next
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

}