package com.eldafito.pokedex.model

import android.net.Uri
import com.eldafito.pokedex.remote.NamedApiResource

open class Pokemon(
    val id: Int,
    val name: String
) {
    companion object {

        fun fromRemote(namedApiResource: NamedApiResource): Pokemon? {
            val id = Uri.parse(namedApiResource.url).lastPathSegment?.toIntOrNull()
            return id?.let { Pokemon(id, namedApiResource.name) }
        }

    }
}
