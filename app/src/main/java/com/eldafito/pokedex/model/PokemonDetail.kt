package com.eldafito.pokedex.model

import com.eldafito.pokedex.remote.response.PokemonRemote

class PokemonDetail(
    id: Int,
    name: String,
    val images: List<String>,
    val types: List<Type>,
    val stats: List<Stat>
) : Pokemon(id, name) {
    companion object {

        fun fromRemote(pokemonRemote: PokemonRemote): PokemonDetail {
            val imgs = pokemonRemote.sprites.run {
                listOfNotNull(
                    frontDefault,
                    frontShiny,
                    frontFemale,
                    frontShinyFemale,
                    backDefault,
                    backShiny,
                    backFemale,
                    backShinyFemale
                )
            }
            val types = pokemonRemote.types.map { Type(name = it.type.name) }
            val stats = pokemonRemote.stats.map {
                Stat(
                    name = it.stat.name,
                    value = it.effort,
                    baseValue = it.baseStat
                )
            }
            return PokemonDetail(
                id = pokemonRemote.id,
                name = pokemonRemote.name,
                images = imgs,
                types = types,
                stats = stats
            )
        }
    }

    data class Type(val name: String) {

        val emoji: String = when (name) {
            "normal" -> "\uD83D\uDE10"
            "fire" -> "\uD83D\uDD25"

            "fighting" -> "\uD83E\uDD4A"
            "water" -> "\uD83D\uDCA7"

            "flying" -> "\u2708\uFE0f"
            "grass" -> "\uD83E\uDD66"

            "poison" -> "\u2623\uFE0F"
            "electric" -> "\u26A1"

            "ground" -> "\uD83C\uDF31"
            "psychic" -> "\uD83D\uDD2E"

            "rock" -> "\uD83E\uDD18"
            "ice" -> "\uD83E\uDDCA"

            "bug" -> "\uD83D\uDC1E"
            "dragon" -> "\uD83D\uDC32"

            "ghost" -> "\uD83D\uDC7B"
            "dark" -> "\uD83D\uDD76"

            "steel" -> "\uD83D\uDD29"
            "fairy" -> "\uD83E\uDDDA"

            else -> "\u2753"
        }

    }

    data class Stat(val name: String, val value: Int, val baseValue: Int)
}