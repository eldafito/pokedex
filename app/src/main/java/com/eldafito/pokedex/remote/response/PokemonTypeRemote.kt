package com.eldafito.pokedex.remote.response

import com.eldafito.pokedex.remote.NamedApiResource

data class PokemonTypeRemote(
    val type: NamedApiResource,
    val slot: Int
)