package com.eldafito.pokedex.remote.response

import com.eldafito.pokedex.remote.NamedApiResource

data class PokemonStatRemote(
    val stat: NamedApiResource,
    val effort: Int,
    val baseStat: Int
)