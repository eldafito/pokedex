package com.eldafito.pokedex.remote

open class ApiResource(val url: String)

open class NamedApiResource(val name: String, url: String) : ApiResource(url)