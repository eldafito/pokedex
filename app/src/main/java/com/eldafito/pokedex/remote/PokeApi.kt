package com.eldafito.pokedex.remote

import com.eldafito.pokedex.remote.response.ListResponse
import com.eldafito.pokedex.remote.response.PokemonRemote
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface PokeApi {

    companion object {

        private const val BASE_URL = "https://pokeapi.co/api/v2/"

        fun create(): PokeApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT).apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }
            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            val gson = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(PokeApi::class.java)
        }

    }

    @GET("pokemon")
    suspend fun getPokemonList(
        @Query("limit") limit: Int? = null,
        @Query("offset") offset: Int = 0
    ): ListResponse<NamedApiResource>

    @GET("pokemon/{id}")
    fun getPokemon(
        @Path("id") id: Int
    ): Call<PokemonRemote>

}