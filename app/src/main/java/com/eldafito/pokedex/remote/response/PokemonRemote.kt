package com.eldafito.pokedex.remote.response

data class PokemonRemote(
    val id: Int,
    var order: Int,
    val name: String,
    var types: List<PokemonTypeRemote>,
    var stats: List<PokemonStatRemote>,
    var sprites: PokemonSpritesRemote
)