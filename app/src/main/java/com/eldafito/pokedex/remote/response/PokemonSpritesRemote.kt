package com.eldafito.pokedex.remote.response

data class PokemonSpritesRemote(
    var frontDefault: String?,
    var frontShiny: String?,
    var frontFemale: String?,
    var frontShinyFemale: String?,
    var backDefault: String?,
    var backShiny: String?,
    var backFemale: String?,
    var backShinyFemale: String?
)