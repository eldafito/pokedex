package com.eldafito.pokedex

import android.app.Application
import com.eldafito.pokedex.di.appModule
import cz.levinzonr.roxie.Roxie
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class Pokedex : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        Roxie.enableLogging(object : Roxie.Logger {
            override fun log(msg: String) {
                Timber.tag("Roxie").d(msg)
            }
        })

        startKoin {
            androidContext(applicationContext)
            modules(appModule)
        }
    }
}