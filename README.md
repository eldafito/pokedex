# Pokedex

Simple Android project able to show a paginated list of pokemon and show its details once selected.

## Technologies

1. [Kotlin](https://kotlinlang.org/) main programming language
2. [Koin](https://insert-koin.io) for DI
3. [Retrofit](https://github.com/square/retrofit) API Client
4. Coroutines
5. [Roxie](https://github.com/ww-tech/roxie) fork [roxie-coroutines](https://github.com/levinzonr/roxie-coroutines) for MVI architecture
6. [Junit](https://junit.org/) and [mockito-kotlin](https://github.com/nhaarman/mockito-kotlin) for Unit Test

## Approach

  1. Integration of as least third-party libraries as possible in favor of built-in components. For this reason, [RxKotlin](https://github.com/ReactiveX/RxKotlin) (consequently standard Roxie lib) or libraries for the paging indicator are not included.
  2.   Pay more attention to clean architecture rather than UI

## Conclusion

This project was developed in the evenings after work so I ask forgiveness in advance for possible more or less serious bugs 😄